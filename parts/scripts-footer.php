  <!--foundation 6 scripts-->
  <script src="js/jquery.js"></script>
  <script src="js/what-input.js"></script>
  <script src="js/foundation.js"></script>
  <script src="js/app.js"></script>
  <!--twentytwenty scripts-->
  <script src="js/jquery.1.10.1.min.js"></script>
  <script src="js/jquery.event.move.js"></script>
  <script src="js/jquery.twentytwenty.js"></script>
  <script>
  $(window).load(function(){
    $(".twentytwenty-container[data-orientation!='vertical']").twentytwenty({default_offset_pct: 0.7});
    $(".twentytwenty-container[data-orientation='vertical']").twentytwenty({default_offset_pct: 0.3, orientation: 'vertical'});
  });
  </script>
  <!--book scripts-->
  <script src="./js/alongthewallstrip.js"></script>

<div id="pictures" class="cell small-12 small-order-1 medium-12 medium-order-2">
    <!--put text here-->
      <div class="orbit" role="region" aria-label="Along The Berlin Wall book pictures" data-orbit data-use-m-u-i="false">
        <div class="orbit-wrapper">
          <div class="orbit-controls">
            <button class="orbit-previous"><span class="show-for-sr">Previous</span>&#9664;&#xFE0E;</button>
            <button class="orbit-next"><span class="show-for-sr">Next</span>&#9654;&#xFE0E;</button>
          </div>
          <ul class="orbit-container">
            <li class="orbit-slide is-active">
              <img src="./assets/book-pictures/AlongTheBerlinWall-book-cover-front.jpg" />
            </li>
            <li class="orbit-slide">
              <img src="./assets/book-pictures/AlongTheBerlinWall-book-010-011.jpg" />
            </li>
            <li class="orbit-slide">
              <img src="./assets/book-pictures/AlongTheBerlinWall-book-060-061.jpg" />
            </li>
            <li class="orbit-slide">
              <img src="./assets/book-pictures/AlongTheBerlinWall-book-112-113.jpg" />
            </li>
            <li class="orbit-slide">
              <img src="./assets/book-pictures/AlongTheBerlinWall-book-124-125.jpg" />
            </li>
            <li class="orbit-slide">
              <img src="./assets/book-pictures/AlongTheBerlinWall-book-cover-back.jpg" />
            </li>
          </ul>
        </div>
      </div>
      <div id="photoinfo" class="grid-x grid-padding-x">
        <p id="download" class="cell medium-auto small-12">
        <p id="download" class="cell medium-auto small-12">
          Download here the pdf 
          <a href="./assets/Along-The-Wall-Strip-Berlin-web-87Mb.pdf">for printing (87 Mb) </a>
          <a href="./assets/Along-The-Wall-Strip-Berlin-web-10Mb.pdf">for screen (10 Mb) </a>
        </p>
        <p id="license" class="cell medium-shrink small-12">
          <a href="https://creativecommons.org/licenses/by-sa/4.0/">CC-BY-SA 4.0 lisence</a>
        </p>
      </div>
    <!--put text untill here-->
  </div>
  <!-- </div> -->

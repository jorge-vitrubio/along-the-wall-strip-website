# Along The Wall Strip - website

Website for the project, exhibition and book "[Along the Wall Strip](https://elglobusvermell.org/along-the-wall-strip/)", Berlin 30 years under transformation. The 45 km of the strip of the Wall that separated Berlin monitored, observed, photographed from south to north in 1997 and 2018

## The software

The file `build.php` will call the `parts/functions.php` and the files to be converted, from CSV or Text to HTML.

The code can be found in the `parts` directory and the content to build the site inside `data`.

The PHP builds a HTML/CSS/JS website inside `static` on each run. The script will erase the content on each run.

The `assets`, `css` and `js` are needed for the static website, will be copied into `static` when running the `build.php`.

Some minor changes have to be done by hand, those are referenced inside `static-needed`:
 - `index.html` was coded by hand, and has to be copied into the root directory for the site to work.
 - `picture.html` and `text.html` are examples for the design making.
 - mods on the first and last picture page navigation and has to be done by hand (first have to get rid of previous link, las picture has to get rid of next link).


### Previous considerations before building:

#### Design considerations:

I would suggest build and design three html static pages: `index.html`, `text.html` and `pictures.html` and use them to define the structure, css and js needed.

Once done, the HTML can be splitted and used in different parts of your php code together with the CSS/JS.

This is what you need to use `build.php`.

#### Framwork usage:

The HTML/CSS/JS was created using the framework [Foundation for Sites](https://get.foundation/). The `gulpfile.js`, `package.son`,`package-lock.json` and `yarn.lock` are used to process `scss/` that will be created into `css/` and the `js/`.

The directories `assets/`, `css/` and `js/`are needed by `build.php`. If you want to use a different HTML/CSS/JS feel free to change its content acordingly with the `parts/` files that cotain the stripped HTML build.


## Authors and acknowledgment
Edited by
 - [El globus vermell](https://elglobusvermell.org/)

A project by
 - [European Observatory on Memories (EUROM)](http://europeanmemories.net/)
 - [Fundació Solidaritat UBMEdited by](http://www.solidaritat.ub.edu/)

Idea, coordinated and developed by
 - [El globus vermell](https://elglobusvermell.org/)

Photographs
 - Joan Vitòria i Codina (May 1997)
 - Carles Serra Hartmann and Joan Vitòria i Codina (July 2018)

Texts
 - Axel Klausmeier, Jordi Guixé i Corominas, Pere Buil Castells, Anna de Torróntegui Gonzàlez and Carles Serra Hartmann

English translation
 - Richard Jacques

Collaborations
 - Sara Altamore, Massimiliano Corbo

Editorial design and web site
 - Jorge Vitoria Rubio [vitrubio.net](https://vitrubio.net)

Book production
 - [latrama.cat](https://latrama.cat)

Acknowledgements
 - Javi Ortega, Antina Sander, Xell Rota, Mar Melchor, Roger Miralles

Rights
 - CC-BY-SA 4.0 for the edition: El globus vermell, 2018
 - CC-BY-SA 4.0 for the texts and images: Their authors

ISBN (printed book)
 - 978-84-09-07094-7

DL (printed book)
 - B 29557-2018i

## License
The software developed to create the static HTML/CSS it is licensed under the GPL v3. The rest of the software is up on their own licenses.



## Project status
This project finalized in 2018

<!-- <div id="bookpage" class="small-12 small-center medium-10 large-10 right"> -->
<div id="" class="cell pagetitle">
    <h2 id="title" class="">Berlin,<br /> a wall that divided,<br /> a memorial that unites</h2>
    <h3 id="author" class="">Jordi Guixé i Corominas</h3>
    <h4 id="authorpossition" class="">Historian and director of the European Observatory on Memories</h3>
  </div>
  <div id="" class="cell textbody">
    <!--put text here-->
    <div class="large-9">
    <p>
      The moon peeped cautiously from behind the searchlight of the lonely old watchtower near Treptower Park in Berlin. A tower that was built to control and divide and was part of the 155 kilometres of the famous wall which, from 1961 until 9 November 1989, divided the people of Berlin, German society, the citizens of Europe and the whole world. The tower stood guard over the great social, human and political prison that was the wall. It also controlled the checkpoint on the beautiful old Oberbaum Bridge. There, not many years ago, repression, surveillance and division were the heaviest burden weighing down the everyday life of the men and women of Berlin.
    </p>
    <p>
      We shall soon be commemorating the thirtieth anniversary of the demolition of that dividing wall. Memorial processes must serve for us to learn and build fairer societies. To unite what the violence of the past tried to separate. That is why we at the European Observatory on Memories, among other initiatives, have published this volume. In order to learn. Through a task of photographic and artistic research, a reflection on the public space, on the traces of a wall ten years after it was almost totally demolished. Those traces, whether modified or not, shape an eternal legacy. An aesthetic symbol that goes beyond the way the city has evolved: considerably. The urban, architectural or physical modification of the traces of the wall speaks to us of that. So do the parts that have not been modified – and how! And they speak to us of ourselves. For memory is the transmission of the past into the present. As the authors of El Globus Vermell collective say, the transmission of the images they have collected since 1997 introduces us into the urban fabric of Berlin through three key aspects, well found and closely linked: the restitching of unexplained traces of the wall; intervention, that is to say, conversion into a monument or a museum; and non-intervention. They are by no means lacking a story; quite the opposite; they are full of interactions with silences, disuses, random improvised marks of time and people in the public space.
    </p>
    <p>
      Our friend and director of the Berlin Wall Memorial Foundation, Axel Klausmeier, reminds us that its enormous task of memory is connected to the present and is seen as a process in the ongoing construction of our democracies. Without that work on the present, memory is petrified and frozen; it does not act on our society. The Berlin Wall Memorial is a living one, in which the city and its people take part, rich in debates and in struggles to ensure the endurance of a memory which is uncomfortable for some but which the citizens did not want to disappear. Like the authors of this work, they wanted to fix the traces of that memory in a particular place: the remains of the Berlin Wall.
    </p>
    <p>
      One of the most active agents in the defence of keeping the remains and a memorial more than 1.7 kilometres long was Manfred Fisher who, from the decree for the demolition of the wall –approved on 13 June 1990– fought to preserve scraps and stretches as a collective public heritage to avoid oblivion and the building of more walls. In the face of the pressure to remove every trace of it Fisher said: “We need some physical, palpable remains in order to understand what happened and what it meant.”
    </p>
    <p>
      As a defender of tangible memory he faced the bulldozers to resist its total disappearance. As did the local people with the first proposal to turn it into a museum, which aimed to replace the original with a “symbolic, mural” sculpture. Those citizen groups brought pressure to bear on the governments that promoted the architectural competitions from 1998-1999, which were decisive for the survival of original parts and their conversion into a memorial.
    </p>
    <p>
      The photographs which the reader can appreciate in this work bring new meaning to and hand on the memorial heritage of the wall. Different kinds of buildings, monuments, archaeological, architectural, artistic, testimonial and museographic interventions are represented here in the recent past, in contrast with the present. Not with any ecumenical intention, but because the authors –I deduce– see it and live it as an open process. According to Klausmeier, “a closed memorial petrifies, a memorial in a constant process of evolution and action dignifies and exemplifies about the present past.”
    </p>
    <p>
      The memorial project, highly concentrated, true, at Bernauerstrasse, continued to evolve, preserving one tower, refurbishing the church as a rebuilt symbol of the past and of local action, or the field of cereals that are still harvested and collected in a symbolic ceremony and truly effective memorial. But as we see in the book, the city is full of “wall”. And since 2009, the Berlin Wall and its memorials –in the plural, since more than 25 other sites of memory are currently managed in the city– are an example and a European and international icon.
    </p>
    <p>
      The story the Berlin Wall tells us is more topical than ever owing to the new waves of exiles and the misfortune of the new walls that are being built all over Europe and on the other continents. The perplexity of the present condemns us to the negligent stupidity of not learning from the lessons of the past, in both the public and political spheres. The Berlin Wall is in demand as a democratic heritage on the five continents for working on memory and human rights. But events also mark the present. The world is full of walls built by the greed of wars or the contemporary racial or social “apartheids” of postmodernity. History is not cyclical, events will never be the same, but we see how the democracies, sometimes so forgetful, are now retrieving spaces for internment and building new walls, where the recent, not so distant, genocidal dictatorships had shut up and repressed human beings.
  </div>
    </p>
    <!--put text untill here-->
  </div>
<!-- </div> -->

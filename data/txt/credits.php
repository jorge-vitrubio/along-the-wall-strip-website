<!-- <div id="bookpage" class="small-12 small-center medium-10 large-10 right"> -->
<div id="" class="cell pagetitle">
    <h2 id="title" class="">Credits</h2>
    <h3 id="author" class=""></h3>
    <h4 id="authorpossition" class=""></h3>
  </div>
  <div id="" class="cell textbody">
    <!--put text here-->
    <div class="large-9">
    <p>
      <dl>
        <dt>Edited by</dt>
          <dd><a href="https://elglobusvermell.org">El globus vermell</a></dd>
        <dt>A project by</dt>
          <dd><a href="http://europeanmemories.net/">European Observatory on Memories (EUROM)</a></dd>
          <dd><a href="http://www.solidaritat.ub.edu/">Fundació Solidaritat UBM</a></dd>
        <dt>Idea, coordinated and developed by</dt>
          <dd><a href="https://elglobusvermell.org">El globus vermell</a></dd>
        <dt>Photographs</dt>
          <dd>Joan Vitòria i Codina (May 1997)</dd>
          <dd>Carles Serra Hartmann and Joan Vitòria i Codina (July 2018)</dd>
        <dt>Texts</dt>
          <dd>Axel Klausmeier, Jordi Guixé i Corominas, Pere Buil Castells, Anna de Torróntegui Gonzàlez and Carles Serra Hartmann</dd>
        <dt>English translation</dt>
          <dd>Richard Jacques</dd>
        <dt>Collaborations</dt>
          <dd>Sara Altamore, Massimiliano Corbo</dd>
        <dt>Editorial design and web site</dt>
          <dd>Jorge Vitoria Rubio <a href="https://vitrubio.net">vitrubio.net</a></dd>
          <!-- <dd>Web site font face, <a href="http://indestructibletype.com/Renner.html">Renner</a> by  <a href="http://indestructibletype.com">indestructible type*</a></dd> -->
        <dt>Book production</dt>
          <dd><a href="http://www.latrama.cat">latrama.cat</a></dd>
        <dt>Acknowledgements</dt>
          <dd>Javi Ortega, Antina Sander, Xell Rota, Mar Melchor, Roger Miralles</dd>
        <dt>Rights</dt>
          <dd><a href="https://creativecommons.org/licenses/by-sa/4.0/">CC-BY-SA 4.0</a> for the edition: El globus vermell, 2018</dd>
          <dd><a href="https://creativecommons.org/licenses/by-sa/4.0/">CC-BY-SA 4.0</a> for the texts and images: Their authors</dd>
        <dt>ISBN (printed book)</dt>
          <dd><a href="https://isbnsearch.org/isbn/9788409070947">978-84-09-07094-7</a></dd>
        <dt>DL (printed book)</dt>
          <dd><a href="#">B 29557-2018</a></dd>
      </dl>
    </p>
  </div>
    <!--put text untill here-->
  </div>
<!-- </div> -->

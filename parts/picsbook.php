          <div id="pictures" class="cell small-12 small-order-1 medium-12 medium-order-2">
            <nav id="nav-pictures" class="">
              <ul class="grid-x grid-padding-x no-bullet">
                <li class="cell shrink small-order-1">
                  <a href="AlongTheWallStrip-1.html" class="bar-first" title="first picture page" >
                    <span class="show-for-sr">First</span>|&#9664;&#xFE0E;
                  </a>
                  <a href="<?php echo $pagename_previous ?>" title="<?php echo $pagename_previous ?>">
                    <span class="show-for-sr">Previous</span>&#9664;&#xFE0E;
                  </a>
                </li>
                <li class="cell shrink small-order-3 text-right">
                  <a href="<?php echo $pagename_next ?>" title="<?php echo $pagename_next ?>">
                      <span class="show-for-sr">Next</span>&#9654;&#xFE0E;
                  </a>
                  <a href="AlongTheWallStrip-66.html" class="bar-last" title="last picture page" >
                    <span class="show-for-sr">Last</span>&#9654;&#xFE0E;|
                  </a>
                </li>
                <li class="cell auto small-order-2 text-center rollover-splitter-selector">
                  <a href="#" onclick="switch_style('roll-over-pictures');return false;">
                    <em class="onepic"><span class="hide">double</span></em>
                  </a>
                  <a href="#" onclick="switch_style('splitted-pictures');return false;">
                    <em class="twopic"><span class="hide">splitted</span></em>
                  </a>
                </li>
              </ul>
            </nav>
            <div id="imagescompared" class="columns">
              <div class="twentytwenty-wrapper twentytwenty-horizontal">
                <div class="twentytwenty-container">
                  <img src="./data/AlongTheWallStrip_1997-2018-fotos/AlongTheBerlinWallStrip-<?php echo $data[$d]["ord"]?>-1997.jpg" />
                  <img src="./data/AlongTheWallStrip_1997-2018-fotos/AlongTheBerlinWallStrip-<?php echo $data[$d]["ord"]?>-2018.jpg" />
                  <div class="twentytwenty-overlay">
                    <div class="twentytwenty-before-label"></div>
                    <div class="twentytwenty-after-label"></div>
                  </div>
                  <div class="twentytwenty-handle" style="left: 562.1px;">
                    <span class="twentytwenty-left-arrow"></span>
                    <span class="twentytwenty-right-arrow"></span>
                  </div>
                </div>
              </div>
              <div id="photoinfo" class="grid-x grid-padding-x">
                <p id="address" class="cell medium-auto small-12">
                  <a href="https://www.openstreetmap.org/?mlat=<?php echo $data[$d]["lat"]?>&mlon=<?php echo $data[$d]["long"]?>#map=16/<?php echo $data[$d]["lat"]?>/<?php echo $data[$d]["long"]?>" target="_blank" title="view on OpenStreetMaps">
                  <?php echo $data[$d]["addr"]?>
                  </a>
                </p>
                <p id="coord" class="cell medium-shrink small-12 ">
                    lat: <?php echo $data[$d]["lat"]?> - long: <?php echo $data[$d]["long"]?>
                  <a href="geo:<?php echo $data[$d]["lat"]?>,<?php echo $data[$d]["long"]?>?=18" target="_blank" title="GPS - geo link">
                    <small class="show-for-small-only">[gps link]</small>
                  </a>
                </p>
              </div>
            </div>
          </div>

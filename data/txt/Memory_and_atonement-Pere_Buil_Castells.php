<!-- <div id="bookpage" class="small-12 small-center medium-10 large-10 right"> -->
<div id="" class="cell pagetitle">
    <h2 id="title" class="">Memory and atonement</h2>
    <h3 id="author" class="">Pere Buil Castells</h3>
    <h4 id="authorpossition" class="">Architect & partner at vora arquitectura, associate lecturer at ETSAV-UPC</h3>
  </div>
  <div id="" class="cell textbody">
    <!--put text here-->
    <div class="large-9">
    <p>
      Any city, observed with interest and curiosity, provides us with an opportunity to read its history through its urban form and architecture. If we pay attention we can see how cities explain themselves: the origins, the stages of growth, the historical episodes, the social and economic dynamics. Cities are social, organic constructions and their endurance makes them repositories of their own history. The physical traces of the architecture and, most of all, of the urban fabrics are difficult to eliminate.
    </p>
    <p>
      That superimposition of various physical and morphological traces makes up their identity, constructed over the years, which will continue to shape itself with the changes in the society that will inhabit them in the future.
    </p>
    <p>
      If cities are living organisms that speak of themselves, the monuments and memorials speak to us especially of the tale society tells of itself; also of what the dominant ideas construct at each moment to shape their collective identity.
    </p>
    <p>
      The tales are ephemeral, changing, like the monuments.
    </p>
    <p>
      The monuments that glorify settings, episodes or heroes that are the constructors of collective pride are a reductive and simplistic way of looking at one’s own history. Biased narratives of their own glorified episodes or characters, free of shadows.
    </p>
    <p>
      The construction of more complex narratives of collective memory is more interesting, especially in the cases of historical episodes that are difficult to digest from a contemporary standpoint.
    </p>
    <p>
      They are the memorials that do not glorify, but commemorate. Reminders of what the community is not proud of, episodes in whose commemoration it questions itself, criticises itself, remembers that it does not want to repeat them and wonders why they happened. Tools for the construction of a morally acceptable collective present and future.
    </p>
    <h4 class="large-12">
      Berlin as a paradigm
    </h4>
    <p>
      Here the case of Berlin is exceptional. To a large extent it has constructed its present identity on the story of its role in the most dramatic history of the 20th century. It has even made it an element of cultural and tourist attraction.
    </p>
    <p>
      Berlin strips itself naked to reveal its dramas and wounds, to acknowledge German guilt before the world and to incorporate those events into the global story of the 20th century. Also to explain the division of the world into two impermeable blocs.
    </p>
    <p>
      The line of the remains of the Berlin Wall explains the division of the city into two for over forty years. As do the urban discontinuities it generated, consequences of the development of the city with that interior frontier.
    </p>
    <p>
      The Wall as a vehicle of atonement allows us to speak of at least three strategies. Three ways of dealing with its physical presence (or absence):
      <ul>
        <li>
          - The monumentalization and museumization of some stretches of the Wall as a fossilized reality, a support for an explanation from the sensibility of the present, commonly accepted but also part of a construction of identity.
        </li>
        <li>
          - The stitching of the city on the void left by the Wall when it was demolished. Here the ‘constructed’ traces are interesting. Above and beyond the commemorative monuments and plaques of all kinds, the small marks are striking, suggestive rather than informative (such as the laying of a line of slabs on the asphalt paving, for example), unexplained traces, distortions of an alien reality that represent the collective will not to forget and to construct its present identity on getting over, but never forgetting, that historical event.
        </li>
        <li>
          - Non-intervention, beyond the museumized stretches and the vanished and redeveloped ones. Those neglected remains that still stand, smothered in graffiti, devoid of any story; remains in the residual public space that confront the anonymous, even indifferent, passer-by and the critical ‘seekers’ with a reality that allows them to reconstruct historical events without an ‘official’ explanation.
        </li>
      </ul>
    </p>
    <p>
      Moreover, in Berlin we find many commemorations of the Holocaust. The way Germany has tried to accept its liability for that tragic historical episode is exemplary. The country has monumentalized it from the preservation, interpretation and erection of new structures to keep alive in the collective memory a tale of guilt and shame, a permanent warning.
    </p>
    <p>
      Germany concentrates atonement for almost all the guilt of the Holocaust, whilst other societies claim to be blameless victims. A reductionist vision of black and white, good and bad. The collective memory is often the result of a wilfully simplistic story.
    </p>
    <p>
      The Holocaust had many accomplices, active and passive, all over Europe. It is simplistic to present the Nazis as the only guilty ones and all other Europeans as innocent victims of a regime and a war. It is all too simple a story.
    </p>
    <p>
      In many parts of Europe, through remains, traces that still endure but which no-one has bothered to incorporate into the respective constructions of collective memory, a more complex, ambiguous, uncomfortable, story still lies hidden. A story which, if brought out, can explain and make all Europeans aware of many events from which we should learn, as Berlin has learned.
    </p>
  </div>
    <!--put text untill here-->
  </div>
<!-- </div> -->

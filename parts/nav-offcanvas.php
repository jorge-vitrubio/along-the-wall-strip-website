            <nav id="offcanvas-full-screen" class="offcanvas-full-screen" data-off-canvas data-transition="overlap">
              <div class="offcanvas-full-screen-inner">
                <button class="offcanvas-full-screen-close" aria-label="Close menu" type="button" data-close>
                  <span aria-hidden="true">&times;</span>
                </button>

                <ul class="offcanvas-full-screen-menu">
                  <li class="">
                    <a class="section-title" href="AlongTheWallStrip-1.html">Pictures</a>
                  </li>
                  <li class="">
                    Presentation
                    <ul class="">
                      <li><a href="Introduction-Axel-Klausmeier.html">Introduction</a></li>
                      <li><a href="Berlin-a_wall_that_divided-a_memorial_that_unites-Jordi_Guixe_Corominas.html">Berlin, a wall that divided,a memorial that unites</a></li>
                    </ul>
                  </li>
                  <li class="">
                    Texts
                    <ul class="">
                      <li><a href="Memory_and_atonement-Pere_Buil_Castells.html">Memory and atonement</a></li>
                      <li><a href="Memorialisation_of_the_past-Collective_memory_or_product_consumption-Anna_de_Torrontegui.html">Memorialisation of the past. Collective memory or product consumption</a></li>
                      <li><a href="Land_marks-Carles_Serra_Hartmann.html">(Land)marks</a></li>
                    </ul>
                  </li>
                  <li class="">
                    <a class="section-title" href="book.html">Book</a>
                  </li>
                  <li class="">
                    About
                    <ul class="">
                      <li><a href="credits.html">Credits<small></small></a></li>
                      <li><a href="authors.html">Authors<small></small></a></li>
                    </ul>
                  </li>
                </ul>
              </div>
            </nav>

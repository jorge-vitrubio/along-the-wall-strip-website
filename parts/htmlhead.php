<!--[if IE 8 ]>    <html class="lt-ie9" lang="en"> <![endif]-->
<!--[if IE 9 ]>    <html class="lt-ie10" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--> <html lang="en"> <!--<![endif]-->
<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>berlin. along the wall strip. 30 years under transformation. - <?php echo (pathinfo($file,PATHINFO_FILENAME)) ?> <?php echo $pnum ?></title>
  <link rel="stylesheet"  type="text/css" href="./css/app.css" />
  <!-- place as first css linked -->
  <!-- <link rel="stylesheet" type="text/css" href="./css/twentytwenty.css" title="compare pictures"/>  -->
  <link rel="alternate stylesheet"  type="text/css" href="./css/twentytwentyroll-over.css" title="roll-over-pictures" />
  <link rel="alternate stylesheet"  type="text/css" href="./css/twentytwentysplitted.css" title="splitted-pictures" />
</head>
<body onload="set_style_from_cookie()">

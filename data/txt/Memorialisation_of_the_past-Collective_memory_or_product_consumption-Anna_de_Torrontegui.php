
<!-- <div id="bookpage" class="small-12 small-center medium-10 large-10 right"> -->
<div id="" class="cell pagetitle">
    <h2 id="title" class="">Memorialisation of the past.<br /> Collective memory or product consumption</h2>
    <h3 id="author" class="">Anna de Torróntegui</h3>
    <h4 id="authorpossition" class="">Social architect and member of the Raons Públiques cooperative</h3>
  </div>
  <div id="" class="cell textbody">
    <!--put text here-->
    <div class="large-9">
    <p>
      On the 9th of November 2019 the world commemorates the 30th anniversary of the fall of the Berlin wall. A wall that for 40 years not only divided the city in two but also Germany and the entire world: Capitalism versus Communism, West versus East.
    </p>
    <p>
      The 30th anniversary is a splendid opportunity to remember themes like the division of the city, the Cold War and all the events that lead up to peaceful reunification in 1989-1990. But this date is also an opportunity to remember the importance of learning from the past in order to be able to live a more conscious present and work towards the construction of a brighter future.
    </p>
    <p>
      This date also made me think about how the memorialization of the past can be used to construct a collective memory and identity. How much can the transformation of the wall trail and its so called “sites or places of memory” be used as enduring elements of remembrance?
    </p>
    <h4>
      Memorialisation of the past
    </h4>
    <p>
      In 2008 Aleida Assmann wrote in one of her articles about the fact that the past used to be regarded as fixed and closed, a stable succession of historical events ordered by age and strongly influenced by the actions and decisions of a few. Currently, though, we are experiencing a shift to what Zygmunt Bauman described as liquid modernity. A time in which the idea of a solid past melts into a liquefied and constantly reconstructed recollection of the past that profoundly determines the future. This paradigmatic shift alerts us of the entangled relationship between history and memory. But it also allows us to reflect upon what Michel Foucault and Colin Gordon called “truth”. The realisation of how the recount of historical events occurred in a time and place has historically been influenced by the interests of the ruling powers.
    </p>
    <p>
      Assmann gives a clear description of how the relationship between history and memory has evolved over time up to a postmodern stage characterised by a new interest in the interactions between both. History can no longer be seen as polarized from memories but rather a form of it. Since the 1980s a new interest in modes of remembering as a form of social and cultural practice has arisen among historians. They started asking questions such as “What is known of the past in the present? Which events from the past are selected and how are they presented? What kinds of commemoration acts are devised?”
    </p>
    <p>
      The ways in which the past can be memorialised can and do take multiple forms. Symbols, texts, images, rites, celebrations or even places can all be used as encoded narrations that give meaning to the present. For instance, Aldo Rossi demonstrated in many of his studies how architecture and urban structures are “elements of continuation” with the urban past. Their formalization and the meaning ascribed to them are strongly intertwined with the identity of the place and consequently with the identity of its inhabitants.
    </p>
    <p>
      The places in which we live and the memories that these distil shape our behaviour and the way we think as much as our experience in them transforms them both physically and symbolically.
    </p>
    <p>
      All memories, individual or collective, are anchored to a spatial framework. But, who decides what this constructed environment is, what and who determines what should be preserved, commemorated or erased?
    </p>
    <h4>
      Preservation as a form of memorialisation
    </h4>
    <p>
      Rem Koolhaas and his studio were approached by Beijing government in 2002 to investigate and define a specific form of preservation for China. The team involved in the project started by looking at the history of preservation in terms of what has worldwide been preserved. They soon realized that “everything we inhabit is potentially susceptible to preservation”. It became the first time that someone was possibly thinking of preservation as a prospective activity instead of proactive, as it had always been. Taking into account the mediocrity of much of our currently constructed environment, they stressed the importance of deciding in advance what will be built for posterity. Consequently, and bearing in mind the dominance of lobbies to keep themes such as authenticity, ancientness and beauty, Koolhaas threw an invitation to imagine a kind of barcode super-imposed over the entire centre of Beijing. The barcode will determined what has to be preserved forever or systematically scraped.
    </p>
    <p>
      However radical this proposition might sound it is also, without a doubt, a much more democratic and impartial way of preserving not only part of the physical environment but also the activities and people hosted in it.
    </p>
    <p>
      Similarly, Christine Boyer strongly criticizes the “pseudo-historical” imagery used in commercial operations of urban renovation in her book “The city of collective memory”. Hilde Heynen also reinforces this view when talking about monuments and memorials in current times: “On the one hand they enjoy an immense popularity, attracting ever more people as visitors, or in more active roles as caretakers and defenders. On the other hand it is often stated that historical consciousness is waning and that the tourist gaze directed at monuments fails to grasp their real meaning as connectors to the past.”
    </p>
    <p>
      Berlin, like many other cities around the world, seems to have found a new source of income through an endless number of tourist activities that somehow claim to memorialize the past. But where does the awareness of a past that can help us to understand and construct our collective memory and identity end and where does it start to be a mere consumption of a product? In a time of mass information how do these mummified spaces and all that is said about them become meaningful to us? Do all visitors of the Berlin wall have a genuine interest in it? And even more importantly, who is deciding what parts of the wall should be preserved and which memories recounted?
    </p>
    <h4>
      Collective memories
    </h4>
    <p>
      Susan Sontag argued that there is no such thing as collective memory. Whilst individual memory is a term everybody feels at ease with, collective memory is a contemporary concept, whose real meaning is not completely clear. Sontag says that all memories are individual and irreproducible as they are created in one’s mind and will die with the person.
    </p>
    <p>
      Collectively we can share the account of individual’s memories, exchange them, select them, think about them, but we cannot collectively remember. Although Aleida Assmann agrees that “experiential memories are embodied and thus they cannot be transferred from one person to another”, she also argues that they can be shared. Once an individual shares a memory with a group, this memory becomes a part of a more complex and inclusive view of the past.
    </p>
    <p>
      When the term “collective memory” was coined for the first time by Maurice Halbwachs, its author connected it with another term: “social frame” to avoid misunderstandings. According to Halbwachs, “no memory is possible outside frameworks used by people living in society to determine and retrieve their recollections”.
    </p>
    <p>
      Each of us, as individuals, is part of distinct groups to which we ascribe, according to a structure of shared concerns, values, customs and experiences that constitute a social frame. Sometimes, though, some social groups such as one's family, country or gender are integrated as part of our identity and we refer to them as “we”. But on occasions the history of the group surpasses our lifespan and then we cannot remember our collective past, we have to memorialise it. In this sense collective memory is a crossover between semantic and episodic memory.
    </p>
    <p>
      Any social group does not “have” a memory of themselves. They need to use memorialisation of symbols, myths, narratives, ceremonies, images, monuments or even places to “make” one. Both the identity and the memory of the group are constructed based on a permanent process of inclusion and exclusion that defines their boundaries.
    </p>
    <h4>
      Group identity in place
    </h4>
    <p>
      “Each aspect, each detail of this place has in itself a meaning intelligible only for the members of the group, because each part of the space corresponds to as many different aspects of the structure and the life of its society, at least as regards whatever is most stable in it.” Halbwachs poses with this statement an interesting counterpoint to a study undertaken by Brian Osborne. His study looked at the strategies used in Canada to integrate people separated by geography, history, ethnicity, class and gender with the aim to consolidate a national identity. Hilde Heynen also supported the idea that places harbour memories and therefore sustain identity.
    </p>
    <p>
      The physical environment in which we live, either its buildings or urban structures, can be seen as a historical theatre, a long-lasting observation of human life which provides us with a sense of continuity and truth. Similarly to the collective memories, places and their inherent meaning are mediated items whose preservation or demolition is at the mercy of the interests of ruling powers, whether political or economic. Perhaps the most eloquent question in the current days is not who should decide which buildings represent better our identity but whether buildings can still express any identity at all. What could be the interest of preserving the facades, for instance, of Buckingham Palace if there was no Monarchy to host and no British citizens felt identified with the values and ideas behind its construction?  Hilde Heynen reminds us that “before the Industrial Revolution, people lived their lives in environments that changed only very slowly. Individuals could experience their own life as a passing instance against a background of permanence and continuity.” Then, the explosion of global growth and the increase of migratory flows implied a shift in the relationship with the past, undoubtedly a shift in the way new societies identify themselves, in an everyday more individualised world.
    </p>
    <h4>
      And now?
    </h4>
    <p>
      In this text we mention how the memorialisation of the past is used to construct collective memories and identity. The 30th anniversary of the fall of the Berlin Wall constitutes an excellent example of this. Berlin is exceptionally interesting because the fall of the Wall left a strip of around 43 kilometres across the city physically empty but full of memories.
    </p>
    <p>
      Our past is not a linear and clearly layered succession of events but rather a complex system of events of which much is still largely unknown. To make an analogy with the world of construction, we could imagine our history as a stone wall whose foundation is the earth itself and the wall is built with all kinds of stones, with different sizes and properties, some local and others from afar. Some stones are purposely cut and others recycled from previous walls. The stones can be arrange and rearrange depending on the needs and interests, as our history has been.
    </p>
    <p>
      Today around 3.5 million people live in Berlin, a population composed of more than 20 different ethnicities, each of them carrying their own memories and identities. This could suggest that perhaps the importance should not be placed on remembering a past that can no longer apply to everyone. As Koolhaas suggests in relation with the preservation of our cities, we should start deciding what part of our collective present is worth preserving and what is not. Could we then perhaps stop fixing our attention on what we have been and start to imagine the community that we would like to become in the future?
    </p>
  </div>
    <!--put text untill here-->
  </div>
<!-- </div> -->

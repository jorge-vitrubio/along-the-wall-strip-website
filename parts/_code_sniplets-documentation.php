<!-- notes for the develpment -->

<!-- Get the Files inside a directory-->

<!-- https://stackoverflow.com/questions/1086105/get-the-files-inside-a-directory -->

<!-- DirectoryIterator usage: (recommended) -->
<?php
foreach (new DirectoryIterator('.') as $file) {
    if($file->isDot()) continue;
    print $file->getFilename() . '<br>';
}
?>
<!-- scandir usage: -->
<?php
$files = scandir('.');
foreach($files as $file) {
    if($file == '.' || $file == '..') continue;
    print $file . '<br>';
}
?>
<!-- opendir and readdir usage: -->
<?php
if ($handle = opendir('.')) {
    while (false !== ($file = readdir($handle))) {
        if($file == '.' || $file == '..') continue;
        print $file . '<br>';
    }
    closedir($handle);
}
?>
<!-- glob usage: -->
<?php
foreach (glob("*") as $file) {
    if($file == '.' || $file == '..') continue;
    print $file . '<br>';
}
?>


<!-- print out fields of csv-->
            <p>
              <div class="large-8 left">
                <small><em>order</em></small> <?php echo $data[$d]["ord"]?><br />
                <small><em>address</em></small> <?php echo $data[$d]["addr"]?><br />
              </div>
              <div class="large-4 right columns">
                <small><em>latitude</em></small> <?php echo $data[$d]["lat"]?><br />
                <small><em>longitude</em></small> <?php echo $data[$d]["long"]?><br />
              </div>
            </p>

<!-- as seen on http://stackoverflow.com/questions/9595367/how-can-i-change-an-inline-css-style-when-clicking-a-link -->
<!-- changing some css properties-->
        <script>
              $(document).ready(function() {
              // ^ This is an event, which triggers once all the document is loaded so that the manipulation is always guaranteed to run.
                 $("#double").click(function() {
                 // ^ Attach a click event to our link
                      $(".twentytwenty-overlay").css({
                        display: "inline"
                        });
                      $(".twentytwenty-handle").css({
                          display: "inline"
                          });
                      // ^ select the div and toggle its display
                 });
                 $("#splitted").click(function() {
                 // ^ Attach a click event to our link
                      $(".twentytwenty-overlay").css({
                        display: "inline"
                        });
                      $(".twentytwenty-handle").css({
                          display: "inline"
                          });
                      // ^ select the div and toggle its display
                 });
        </script>
        <!--html-->
        <li class="small-3 text-right">
          <a href="#" id="double" title="compare">
            <em class="onepic"><span class="hide">double</span></em>
          </a>
        </li>
        <li class="small-3">
          <a href="#" id="splitted" title="2 img">
            <em class="twopic"><span class="hide">splitted</span></em>
          </a>
        </li>


<!-- as seen on http://stackoverflow.com/questions/9595367/how-can-i-change-an-inline-css-style-when-clicking-a-link -->
<!-- loading a new css -->
      <script>
        $(document).ready(function() {
        // ^ This is an event, which triggers once all the document is loaded so that the manipulation is always guaranteed to run.
           $("#double").click(function() {
             // ^ Attach a click event to our link
               $( "<style type=\"text/css\">
                       .twentytwenty-handle,
                       .twentytwenty-overlay {
                         display:inline
                       }
                       img.twentytwenty-before {
                       }
                       img.twentytwenty-after {
                       }
                   </style>")
               .appendTo( "head" );
           });

           $("#splitted").click(function() {
           // ^ Attach a click event to our link
                $( "<style type=\"text/css\">
                        .twentytwenty-handle,
                        .twentytwenty-overlay {
                          display:none
                        }
                        img.twentytwenty-before {
                          clip: unset !important;
                        }
                        img.twentytwenty-after {
                        }
                    </style>")
                .appendTo( "head" );
           });
        });
      </script>

      <!-- html-->
      <li class="small-3 text-right">
        <a href="#" id="double" title="compare">
          <em class="onepic"><span class="hide">double</span></em>
        </a>
      </li>
      <li class="small-3">
        <a href="#" id="splitted" title="2 img">
          <em class="twopic"><span class="hide">splitted</span></em>
        </a>
      </li>



<!--http://stackoverflow.com/questions/19844545/replacing-css-file-on-the-fly-and-apply-the-new-style-to-the-page -->

        <script>
        function changeCSS(cssFile, cssLinkIndex) {
            var oldlink = document.getElementsByTagName("link").item(cssLinkIndex);

            var newlink = document.createElement("link");
            newlink.setAttribute("rel", "stylesheet");
            newlink.setAttribute("type", "text/css");
            newlink.setAttribute("href", cssFile);

            document.getElementsByTagName("head").item(0).replaceChild(newlink, oldlink);
        }
        </script>

        <!--The HTML: -->

<html>
    <head>
        <title>Changing CSS</title>
        <link rel="stylesheet" type="text/css" href="../css/twentytwenty.css" /> <!-- place as first css linked -->
    </head>
    <body>

      <li class="small-3 text-right">
        <a href="#" onclick="changeCSS('../css/twentytwenty.css', 0);">
          <em class="onepic"><span class="hide">double</span></em>
        </a>
      </li>
      <li class="small-3">
        <a href="#" onclick="changeCSS('../css/twentytwentysplitted.css', 0);">
          <em class="twopic"><span class="hide">splitted</span></em>
        </a>
      </li>

    </body>
</html>

// htmls body for index class 
//
// class="<?php if ($file = index.php){echo 'index';}?>"
//
//

<!-- <div id="bookpage" class="small-12 small-center medium-10 large-10 right"> -->
<div id="" class="cell pagetitle">
    <h2 id="title" class="">Introduction</h2>
    <h3 id="author" class="">Axel Klausmeier</h3>
    <h4 id="authorpossition" class="">Director of the Berlin Wall Memorial</h3>
  </div>
  <div id="" class="cell textbody">
    <!--put text here-->
    <div class="large-9">
      <p>
        In the days following August 13, 1961, when the GDR began building the wall and sealing off East Berlin, few could imagine that 28 years, 2 months and 27 days would pass before the Berlin Wall would be peacefully overcome, making German unification and freedom in the fall of 1989 possible again. It was also unimaginable that the most visible symbol of the Cold War would cost at least 140 lives in Berlin alone.
      </p>
      <p>
        In February 2018, we commemorated 10,315 days with and 10,315 days without the Berlin Wall. During its 28-year existence, the Berlin Wall underwent constant change; it expanded and in a perfidious sense improved, becoming more effective at preventing escapes. Similarly, after the political fall of the wall, the one-time barrier and security strip also underwent a transformation. Political, local, urban planning and social factors, like the “Capital Resolution” of 1991, were responsible for this change. It was expected that a construction boom would follow, accompanied by an unprecedented influx of new residents to the capital, but this did not happen immediately.
      </p>
      <p>
        Even before Germany was politically unified in October 1990, the first sections of the wall had already been deemed worthy of conservation as historical monuments; by the end of 1990, only 12 relics of the Berlin Wall had been added to the Berlin monuments list. The general mood in the city was dominated by the constantly repeated chant “The wall must go!” and thus, Berliners followed the words of their former mayor Willy Brandt, who had been demanding exactly that since 1961.
      </p>
      <p>
        Today, in the 28th year since the fall of the wall, some 30 pieces of the wall have been listed as historical monuments. Some, such as the sections on Rudower Höhe and on Zimmerstrasse at the Topography of Terror, have required special protection with a fence. At the Berlin Wall Memorial on Bernauer Strasse, the large, thick metal walls enclosing the sides of the official “Monument” also attest to the long debate over whether to preserve the wall as a monument, a discussion that continued for more than a decade before this goal was achieved politically.
      </p>
      <p>
        The speedy demolition of the border fortifications in 1990 created several “non-sites” illustrated by the pictures in this publication. The emptiness that was left behind became a “shadow monument” and reminder that this had once been a historic site of international significance. Now overgrown with vegetation, it has become banal.
      </p>
      <p>
        Meanwhile, Berlin continues to boom and develop: the grounds around the former border strip in some ways reflect other developments taking place in the city: here urban density and the optimization of floor area values, there recreational opportunities and the establishment of commemorative sites and memorial columns. Visitors may still encounter urban wastelands at a few sites, but these areas are becoming fewer. For those with knowledge of the history, a few areas of the former border strip function as memory storage sites. At the same time, in recent years the state and city of Berlin has become increasingly aware of its duty to remember the past. The most frequently asked question by Berlin tourists from all over the world continues to be: “Where was the wall?” At several sites, such as the Berlin Wall Memorial on Bernauer Strasse, this question is answered exemplarily.
      </p>
      <p>
        There is still much to be done, however, in the area of political commemoration. This book takes a step in this direction by once again addressing this unusual heritage.
      </p>
    </div>
    <!--put text untill here-->
  </div>
<!-- </div> -->

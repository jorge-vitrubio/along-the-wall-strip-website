<!-- <div id="bookpage" class="small-12 small-center medium-10 large-10 right"> -->
<div id="" class="cell pagetitle">
    <h2 id="title" class="">Authors</h2>
  </div>
  <div id="" class="cell textbody">
    <!--put text here-->
    <div class="large-9">
    <p>
      <dl>
        <dt>Idea, coordination and development</dt>
        <dd>
          <dl>
            <dt>El globus vermell</dt>
            <dd><a href="https://www.elglobusvermell.org">www.elglobusvermell.org</a> | info@elglobusvermell.org</dd>
            <dd>An architects collective based in Barcelona that has been working since 2009 to spread knowledge about the built environment and produce critical and demanding citizens.</dd>
          </dl>
        </dd>
        <dt>Photographs</dt>
        <dd>
          <dl>
            <dt>Joan Vitòria i Codina (1973)</dt>
            <dd>joanvc@elglobusvermell.org</dd>
            <dd>Architect from the Escola d’Arquitectura de Barcelona (ETSAB-UPC). He studied for one year at the Universität der Künste (UdK) in Berlin. He has worked as an independent architect and in studios in Barcelona and Paris. From 2007 to 2013 he was a lecturer in Projects at the Escola d’Arquitectura (ESARQ-UIC, Barcelona) and coordinator of the Department of Culture and Publications. He curated the exhibition “Matèria sensible. Joves arquitectes catalans”, which travelled around European cities, among them Berlin, from 2010 to 2012. He is a founder member of El globus vermell.</dd>
          </dl>
        </dd>
        <dd>
          <dl>
            <dt>Carles Serra Hartmann (1978)</dt>
            <dd>carlesserrahartmann@hotmail.com</dd>
            <dd>Graduate in Law from the Universitat Pompeu Fabra (UPF) and architect from the Escola d’Arquitectura del Vallès (ETSAV-UPC). He studied for one year at the Universität der Künste (UdK) in Berlin. Since then he has combined teaching at the Universitat Politècnica de Catalunya (UPC) Department of Architectural Composition with activities with “El globus vermell”. German architecture between the wars and the architecture of Berlin in particular have been the focus of much of his research. He lives and works in Berlin.</dd>
          </dt>
        </dd>
        <dt>Articles</dt>
        <dd>
          <dl>
            <dt>Pere Buil Castells (1973)</dt>
            <dd>pere.buil@vora.cat</dd>
            <dd>Architect from the Escola d’Arquitectura de Barcelona (ETSAB-UPC). Founder partner of the vora studio (<a href="https://www.vora.cat">www.vora.cat</a>). Some of his projects have won awards and been published in international journals. He is currently a lecturer at the Escola d’Arquitectura del Vallès (ETSAV-UPC). He has given classes at ETSALS La Salle, ESARQ-UIC and BIArch. Joint curator of the exhibition “Matèria sensible. Joves arquitectes catalans” (2010-2012) and the season of colloquia “Estacions transformadores” (2008-2010).</dd>
          </dl>
        </dd>
        <dd>
          <dl>
            <dt>Anna de Torróntegui González (1982)</dt>
            <dd>annadetorrontegui@gmail.com</dd>
            <dd>Architect from the Escola d’Arquitectura del Vallès (ETSAV-UPC) and a PhD in Environmental Psychology from the University of Surrey (United Kingdom). She has combined work in architecture in Barcelona and London with contributions to El globus vermell from 2011 to 2014. She is currently a member of the Raons Públiques cooperative in Barcelona.</dd>
          </dl>
        </dd>
        <dd>
          <dl>
            <dt>Carles Serra Hartmann (1978)</dt>
          </dl>
        </dd>
        <dt>Book and website</dt>
        <dd>
          <dl>
            <dt>Jorge Vitoria Rubio (1973)</dt>
            <dd><a href="https://vitrubio.net">www.vitrubio.net</a> | jorge@vitrubio.net</dd>
            <dd>Graphic and designer, publishing specialist, website developer, freesoftware advocate, photography enthusiast, bicycle fanatic and tireless traveller.</dd>
          </dl>
        </dd>
        <dt>Collaborators</dt>
        <dd>
          <dl>
            <dt>Sara Altamore (1988)</dt>
            <dd>saraaltamore@gmail.com</dd>
            <dd>Graduate in Civil Engineering and Architecture from Catania in 2013. In 2014 she worked with El globus vermell. In 2015 she did a master’s degree in Sustainability of the Territory. The same year she began to devote herself to research and teaching from the LabPEAT laboratory at the Università di Catania. She is currently preparing her doctorate in Regional Planning and Public Policies at the Istituto Universitario di Architettura in Venice.</dd>
          </dl>
        </dd>
        <dd>
          <dl>
            <dt>Massimiliano Corbo (1980)</dt>
            <dd>studio.massimiliano.corbo@gmail.com</dd>
            <dd>Architect from the Istituto Universitario di Architettura in Venice. He has worked as an independent architect and in studios in Bologna. In 2015 he did a course in higher professional training as senior technician for Communication and Multimedia, thanks to which he was able to take part in the Erasmus+ European mobility project and work with El globus vermell.</dd>
          </dl>
        </dd>
      </dl>
    </p>
  </div>
    <!--put text untill here-->
  </div>
<!-- </div> -->

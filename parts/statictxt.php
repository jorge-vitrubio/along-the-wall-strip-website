<?php require 'htmlhead.php'; ?>
  <div class="off-canvas-wrapper">
    <div class="off-canvas-content" data-off-canvas-content>
      <?php require 'header.php'; ?>
      <section class="grid-container" role="document" >
        <div id="bookpage" class="grid-x grid-padding-x">
          <?php  require ($filefolder.$file) ?>
        </div>
      </section>
      <?php require 'footer.php'; ?>
    </div>
    <!--end off-canvas-content -->
    <?php require 'nav-offcanvas.php'; ?>
  </div>
  <!-- off-canvas-wrapper -->
  <?php require 'scripts-footer.php'; ?>
</body>
</html>

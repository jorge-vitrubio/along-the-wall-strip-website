<?php

function save_each_file($pagename='',$cached_content_data=''){
  //check dir and create it if neccessary
  if (!is_dir('static/')) {
    // dir doesn't exist, make it
    mkdir('static');
  }
  //output the static html file
  file_put_contents('static/'.$pagename,$cached_content_data);
}

function csv_to_html($filename='', $delimiter='',$fileoutname='',$htmltemplate=''){
  if(!file_exists($filename) || !is_readable($filename))
  return FALSE;
  $header = NULL;
  $data = array();
  if (($handle = fopen($filename, 'r')) !== FALSE){
    while (($row = fgetcsv($handle, 1000, $delimiter)) !== FALSE){
      $numpos = count($data);
      if(!$header)
      $header = $row;
      else
      $data[] = array_combine($header, $row);
    }
    fclose($handle);
  }
  for ($d=0;$d<$numpos; $d++){
    ob_start();
    $pnum = $d;
    $pnum++;
    $pnumprev = $pnum;
    $pnumprev--;
    $pnumnext = $pnum;
    $pnumnext++;
    $pagename = "$fileoutname$pnum.html";
    $pagename_previous = "$fileoutname$pnumprev.html";
    $pagename_next = "$fileoutname$pnumnext.html";
    include $htmltemplate;
    $cached_content_data = ob_get_contents();
    ob_end_clean();
    save_each_file($pagename,$cached_content_data);
  }
}

function txt_to_html($filefolder='',$htmltemplate='',$pnum=''){
  foreach (new DirectoryIterator($filefolder) as $file) {
    if(!$file->isDot()){
      ob_start();
      include $htmltemplate;
      $pagename = pathinfo($file,PATHINFO_FILENAME).".html";
      $cached_content_data = ob_get_contents();
      ob_end_clean();
      save_each_file($pagename,$cached_content_data);
    }
  }
}
?>
